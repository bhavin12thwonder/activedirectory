﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using System.IO;

namespace ActiveDirectory
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string DomainName = ConfigurationManager.AppSettings["Domain"];
            string DomainContainer = ConfigurationManager.AppSettings["DefaultOU"];
            string ADGroupName = "";
            string User = ConfigurationManager.AppSettings["ServiceUser"];
            string Password = ConfigurationManager.AppSettings["ServicePassword"];
            List<string> GroupName;


            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\ADusers.txt");



            // PrincipalContext yourOU = new PrincipalContext(ContextType.Domain, DomainName, DomainContainer);
            //GroupPrincipal findAllGroups = new GroupPrincipal(yourOU, "*");
            //PrincipalSearcher ps = new PrincipalSearcher(findAllGroups);
            //foreach (var group in ps.FindAll())
            //{
            //    file.WriteLine(group.Name);
            //}
            //file.Close();


            using (var context = new PrincipalContext(ContextType.Domain, DomainName, DomainContainer, ContextOptions.SimpleBind, User, Password))
            {
                using (var grp = GroupPrincipal.FindByIdentity(context, IdentityType.SamAccountName, ADGroupName))
                {
                    if (grp != null)
                    {
                        GroupName = new List<string>();
                        foreach (var member in grp.GetMembers())
                        {
                            GroupName.Add(member.SamAccountName);
                            Console.WriteLine(member.SamAccountName + " - " + member.DisplayName);
                            file.WriteLine(member.SamAccountName + " - " + member.DisplayName + " - " + grp.Name);
                        }
                    }
                }
            }
            file.Close();
            Console.Write("----------------------------------");
           // Console.Write("All Members in list :" + GroupName);
        } 

       
                  
    }
}
